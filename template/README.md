# Google protobuf compiler protoc @VERSION@

Python wheels for the Google protocol buffer compiler
[protoc @VERSION@](https://github.com/protocolbuffers/protobuf/releases/tag/v@VERSION@).

Google Protocol buffers are a language-neutral, platform-neutral
extensible mechanism for serializing structured data.

See https://developers.google.com/protocol-buffers/ for more information.
