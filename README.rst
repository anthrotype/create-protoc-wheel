=========================================
 Package protoc as Python wheel packages
=========================================

`create_protoc_wheel` is a tool to generate Python wheel packages for
the Google protocol buffer compiler `protoc`.

Google Protocol buffers are a language-neutral, platform-neutral
extensible mechanism for serializing structured data.

See https://developers.google.com/protocol-buffers/ for more information.


Usage
=====

.. code-block:: shell

   ./create-protoc-wheel <protobuf version>
